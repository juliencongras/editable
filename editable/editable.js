
let echoJson = '../test/up.php'
let monTab;
let tableElem = document.createElement('table')
let th = document.createElement('th')
let tr = document.createElement('tr')
let td = document.createElement('td')

let tbody = document.createElement('tbody')

function tableHeader(){
    let thead = document.createElement('thead')
    let unElement = monTab[0]
    let tr = document.createElement('tr')
    for (let key in unElement){
        let th = document.createElement('th')
        tr.appendChild(th)
        th.innerText = key
        th.setAttribute("onclick", "triDuTableau()");
    }
    thead.appendChild(tr)
    tableElem.appendChild(thead)
}

function tableBody(){
    let tbody = document.createElement('tbody')
    for (let obj of monTab){
        let tr = document.createElement('tr')
        for (let val of Object.values(obj)){
            let td = document.createElement('td')
            td.innerText = val
            tr.appendChild(td)
            td.setAttribute("contenteditable", "true");
        }
        tbody.appendChild(tr)
        tr.classList.add('rowtbody')
    }
    tableElem.appendChild(tbody)
}

function createTable(elem){
    elem.appendChild(tableElem)
    let jsonURL = elem.getAttribute('data-source')
    fetch(jsonURL)   // j'envoie ma requête
        .then(      // quand je reçois la réponse
            (response) => response.json() // je récupère le JSON de la réponse
        )
        .then(                            // quand c'est pret
            (tab) => {
                monTab = tab;           // je mets mes data dans ma variable
            
                //creerMonTableauHtml(monTab);      // je lance ma fonction d'affichage
                tableHeader()
                tableBody()
            }
        ).catch(
            (err) => {
                console.log(err);          // j'affiche les erreurs HTTP au cas où
            },
        );
}

function triDuTableau(){
    let tableTri = document.querySelectorAll('tr')
    let tableTri2 = Array.from(tableTri)
    tableTri2.sort(function(a, b) {
        return a - b;
    });
}

function ajoutligne(){
    var tbodyRef = document.getElementsByTagName('tbody')[0];
    var newRow = tbodyRef.insertRow();
    var thtable = document.getElementsByTagName('th');

    for (let n of thtable){
        var newCell = newRow.insertCell();
        var newText = document.createTextNode('Modifier');
        newCell.appendChild(newText);
        newCell.setAttribute("contenteditable", "true");
        newRow.classList.add('rowtbody')
    };
    
}

function sauvegarde(){
    let table = document.querySelector('table')
    let JSONtable =  JSON.stringify(table.innerText)
    console.log(JSONtable);
    
}

/*function sauvegarde(){
    let table = document.querySelector('table')
    (async () => {
        const rawResponse = await fetch('../test/up.php', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(table.innerText)
        });
        const content = await rawResponse.json();
      
        console.log(content);
      });
}*/

function filtredutableau(){
    rows = document.getElementsByClassName('rowtbody')
    for (row of rows){
        inputfiltre = document.getElementById('filtre').value;
        if (row.innerText.includes(inputfiltre) == false){
            row.style.display = "none"
        }
        else{
            row.style.display = ""
        }
    }
}
